from django.db import models
from django.contrib.postgres.fields import ArrayField

# Create your models here.
class Author (models.Model):
    name = models.CharField(max_length=255, blank=False)
    description = models.CharField(max_length=255, blank=False)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    def __str__ (self):
        return "{}".format(self.name)

class BlogPost (models.Model):
    title = models.CharField(max_length=255, blank=False)
    summary = models.CharField(max_length=500)
    body = models.TextField()
    tags = ArrayField(models.CharField(max_length=10, blank=True), size=5)
    author = models.ForeignKey('Author', on_delete=models.CASCADE)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    def __str__ (self):
        return "{}".format(self.title)
