from rest_framework import serializers
from .models import Author, BlogPost

class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = ('id', 'name', 'description', 'date_created', 'date_modified')
        read_only_fields = ('date_created', 'date_modified')

class BlogPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = BlogPost
        fields = ('id', 'title', 'summary', 'body', 'tags', 'author', 'date_created', 'date_modified')
        read_only_fields = ('date_created', 'date_modified')